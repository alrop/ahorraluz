//
//  PricesSource.swift
//  AhorraLuz
//
//  Created by Alejandro Portillo Guerrero on 14/02/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import Foundation

class PricesSource {
    var data: [Double]
    var dataNormalized: [Double]!
    
    init(data: [Double]) {
        self.data = data
        
        normalizeData()
    }
    
    func getConvenientHour() -> Int? {
        
        var (start, end) = Settings.retrieveInterval()
        
        guard start < data.count else { return nil }
        guard end <= data.count else { return nil }
        
        if start == end {
            end = start + 2
        }
        
        let newPrices = data[start...end - 1]
        
        guard let bestIndex = getBestIndex(array: Array(newPrices)) else { return nil }
        
        return start + bestIndex
    }
    
    func getBestHour() -> Int? {
        return getBestIndex(array: data)
    }
    
    func getWorstHour() -> Int? {
        return getWorstIndex(array: data)
    }
    
    func normalizeData() {
        if let min = data.min() {
            dataNormalized = data.map({$0 - min})
        }
    }
}

private extension PricesSource {
    func getBestIndex(array: [Double]) -> Int? {
        guard let bestTime = array.min() else { return nil }
        guard let index = array.index(of: bestTime) else { return nil }
        
        return index
    }
    
    func getWorstIndex(array: [Double]) -> Int? {
        guard let worseTime = array.max(),
            let index = array.index(of: worseTime) else { return nil }
        
        return index
    }
}
