//
//  UIViewController+StoryboardIdentifiable.swift
//  doozy-ios-app
//
//  Created by Alejandro Guerrero on 31/08/2016.
//  Copyright © 2016 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit

extension StoryboardIdentifiable where Self: UIViewController {
    static var storyboardIdentifier: String {
        return String(describing: self)
    }
}

extension UIViewController : StoryboardIdentifiable { }
