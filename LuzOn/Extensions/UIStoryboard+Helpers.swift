//
//  UIStoryboard+Helpers.swift
//  doozy-ios-app
//
//  Created by Alejandro Guerrero on 31/08/2016.
//  Copyright © 2016 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    enum Storyboard : String {
        case Main
    }
    
    
    convenience init(storyboard: Storyboard, bundle: Bundle? = nil) {
        self.init(name: storyboard.rawValue, bundle: bundle)
    }
    
    
    class func storyboard(storyboard: Storyboard, bundle: Bundle? = nil) -> UIStoryboard {
        return UIStoryboard(name: storyboard.rawValue, bundle: bundle)
    }
    
    
    func instantiateViewController<T: UIViewController>() -> T where T: StoryboardIdentifiable {
        guard let viewController = self.instantiateViewController(withIdentifier: T.storyboardIdentifier) as? T else {
            fatalError("Couldn't instantiate view controller with identifier \(T.storyboardIdentifier) ")
        }
        
        return viewController
    }
}
