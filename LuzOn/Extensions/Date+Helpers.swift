//
//  Date+Helpers.swift
//  LuzOn
//
//  Created by Alejandro Portillo Guerrero on 27/06/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import Foundation

extension Date {
    
    static var calendar: Calendar {
        get {
            return Calendar(identifier: .gregorian)
        }
    }
    
    static func getMidnight() -> Date {
        return calendar.startOfDay(for: Date())
    }
}
