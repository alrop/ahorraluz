//
//  SlideFromSegue.swift
//  AhorraLuz
//
//  Created by Alejandro Portillo Guerrero on 18/02/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit

class SlideFromSideSegue: UIStoryboardSegue {
    let transition = SlideFromTransitionManager()
    
    override func perform() {
        destination.transitioningDelegate = transition
        super.perform()
    }
}

class SlideFromLeftSegue: SlideFromSideSegue {}

class SlideFromRightSegue: SlideFromSideSegue {
    override func perform() {
        transition.slideFrom = .right
        super.perform()
    }
}
