//
//  MonthSelectorDataSource.swift
//  AhorraLuz
//
//  Created by Alejandro Portillo Guerrero on 13/02/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit

protocol MonthSelectorDataSourceDelegate: class {
    func monthTap(_ position: Int)
}

class MonthSelectorDataSource: NSObject {
    
    weak var delegate: MonthSelectorDataSourceDelegate?
    
    var info: [Int: (String, Int)]?
    
    func configureTitles() {
        let currentMonth = Calendar.current.component(.month, from: Date()) - 1
        
        info = [Int: (String, Int)]()
        
        for i in 0...12 {
            let position = (currentMonth + i + 12) % 12
            
            let name = EcoFormatter.dateFormatter.standaloneMonthSymbols[position].capitalized
            
            info?[i] = (name, position)
        }
    }
}
    
extension MonthSelectorDataSource: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return info?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: MonthSelectorCollectionViewCell = collectionView.dequeueReusable(forIndexPath: indexPath)
        
        cell.titleLabel.text = info?[indexPath.row]?.0
        cell.monthPosition = info?[indexPath.row]?.1
        
        return cell
    }
}

extension MonthSelectorDataSource: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let (_, position) = info?[indexPath.row] else { return }

        delegate?.monthTap(position)
    }
}
