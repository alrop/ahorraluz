//
//  Parser.swift
//  AhorraLuz
//
//  Created by Alejandro Portillo Guerrero on 19/01/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import Foundation

protocol ParserDelegate: class {
    func didFinishParsing(withPrices prices: PricesSource)
}

enum ElectricityPlan: Int {
    case standard
    case nights
    case electricCar
    
    var acronym: String {
        switch self {
        case .standard:
            return "Z01"
        case .nights:
            return "Z02"
        case .electricCar:
            return "Z03"
        }
    }
}

class Parser: NSObject {
    fileprivate var dateToSearch = ""
    fileprivate var plan: ElectricityPlan = .standard
    
    fileprivate var prices = [Double]()
    
    fileprivate var terminoCosteHorario = false
    fileprivate var tipoPrecio = false
    
    fileprivate var parser: XMLParser?
    
    weak var delegate: ParserDelegate?
    
    convenience init?(dateToSearch: String, typeOfService: ElectricityPlan = .standard) {
        self.init()
        
        self.dateToSearch = dateToSearch
        self.plan = typeOfService

        initParser()
    }
    
    func parse() -> Bool {
        guard let parser = parser else { return false }
        return parser.parse()
    }
}

fileprivate extension Parser {
    func initParser() {
        
        let urlString = "https://api.esios.ree.es/archives/80/download?date_type=datos&end_date="+dateToSearch+"T23%3A59%3A59%2B00%3A00&locale=es&start_date="+dateToSearch+"T00%3A00%3A00%2B00%3A00"
        
        guard let url = URL(string: urlString) else { return }
        
        requestData(url)
    }
    
    func requestData(_ url: URL) {
        URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
            if let data = data {
                self?.parser = XMLParser(data: data)
                self?.parser?.delegate = self
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: { [weak self] in
                    self?.parser?.parse()
                })
//                DispatchQueue.main.async { [weak self] in
//                }
            }
        }.resume()
    }
}

extension Parser: XMLParserDelegate {
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        
        if elementName == "TerminoCosteHorario" && attributeDict["v"] == "FEU" {
            terminoCosteHorario = true
        }
        
        if elementName == "TipoPrecio" && attributeDict["v"] == plan.acronym {
            tipoPrecio = true
        }
        
        if terminoCosteHorario && tipoPrecio {
            if elementName == "Ctd", let value = attributeDict["v"] {
                if let value = Double(value) {
                    prices.append(Double(value))
                }
            }
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if elementName == "SeriesTemporales" {
            terminoCosteHorario = false
            tipoPrecio = false
        }
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        let data = PricesSource(data: prices)
        delegate?.didFinishParsing(withPrices: data)
    }
}
