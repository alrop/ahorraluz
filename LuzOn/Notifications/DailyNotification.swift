//
//  DailyNotification.swift
//  doozy-ios-app
//
//  Created by Alejandro Guerrero on 24/10/2016.
//  Copyright © 2016 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit
import UserNotifications

protocol LocalNotification {
    @available(iOS 10.0, *)
    var request: UNNotificationRequest {get set}
}

@available(iOS 10.0, *)
class DailyNotification: LocalNotification {
    
    var request: UNNotificationRequest
    
    init(date: Date) {
        
        let dateComponents = EcoFormatter.calendar.dateComponents([.hour, .minute], from: date)
        
        let content = UNMutableNotificationContent()
        content.title = NSLocalizedString("¡Nuevos Datos Disponibles!", comment: "")
        content.sound = UNNotificationSound.default()
        
        content.body = NSLocalizedString("Entra para emepzar a ahorrar", comment: "")
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
        request = UNNotificationRequest(identifier: String(describing: DailyNotification.self), content: content, trigger: trigger)
    }
}

class DailyNotificationiOS9: UILocalNotification {
    init(date: Date) {
        super.init()
        
        fireDate = date
        repeatInterval = .day
        alertTitle = NSLocalizedString("¡Nuevos Datos Disponibles!", comment: "")
        alertBody = NSLocalizedString("Entra para emepzar a ahorrar", comment: "")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
