//
//  MainViewController.swift
//  AhorraLuz
//
//  Created by Alejandro Portillo Guerrero on 18/01/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit
import Charts
import FirebaseAnalytics

class MainViewController: UIViewController, TransparentNavigationBar {
    @IBOutlet weak var bestTimeLabel: UILabel!
    @IBOutlet weak var worstTimeLabel: UILabel!
    @IBOutlet weak var convenientTimeLabel: UILabel!
    @IBOutlet weak var gradientLabel: GradientView!
    
    @IBOutlet weak var chartView: ChartView!
    
    @IBOutlet weak var nextButton: EditableView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    fileprivate var currentDate = Date()
    fileprivate var prices: PricesSource?
    fileprivate var dataSource: MonthSelectorDataSource?
    
    @IBOutlet weak var hoursDraggingView: HoursDraggingSelectorView!
    @IBOutlet weak var leftHandle: UIImageView!
    @IBOutlet weak var rightHandle: UIImageView!
    @IBOutlet weak var hoursSeparatorView: HoursSeparatorView!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var dateLabel: UIButton!
    @IBOutlet weak var pickerView: UIView!
    @IBOutlet weak var pickerViewBottom: NSLayoutConstraint!
    @IBOutlet weak var loader: UIImageView!
    
    var parser: Parser?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initParser(dateToSearch: currentDate)
        configureCollectionView()
        
        hoursSeparatorView.delegate = self
        
        makeNavigationBarTransparent()
        
        if UIScreen.main.bounds.size.height > 568 {
            leftHandle.translatesAutoresizingMaskIntoConstraints = true
            rightHandle.translatesAutoresizingMaskIntoConstraints = true
            hoursDraggingView.translatesAutoresizingMaskIntoConstraints = true
        }
        
        datePicker.maximumDate = Date()
        
        //start animation
        var images = [UIImage]()
        
        for i in 1...30 {
            if let image = UIImage(named: "rayo\(i)") {
                images.append(image)
            }
        }
        
        loader.animationImages = images
        loader.animationDuration = 1
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureUI()
        loadHandlersPosition()
    }
    
    func initParser(dateToSearch: Date) {
        nextButton.isEnabled = enableNextDay()
        
        configureSearchUI()
        
        currentDate = dateToSearch
        
        dateLabel.setTitle(EcoFormatter.visualFormatter.string(from: dateToSearch).capitalized, for: .normal)
        
        let date = EcoFormatter.dateFormatter.string(from: dateToSearch)
        parser = Parser(dateToSearch: date)
        
        guard let parser = parser else { return }
        parser.delegate = self
    }
    
    func configureSearchUI(_ enable: Bool = true) {
        DispatchQueue.main.async {
            if enable {
                self.loader.startAnimating()
            } else {
                self.loader.stopAnimating()
            }
        }
        
        let smallScreens = UIScreen.main.bounds.size.height <= 568
        
        leftHandle.isHidden = smallScreens || enable
        rightHandle.isHidden = smallScreens || enable
        hoursDraggingView.isHidden = smallScreens || enable
        chartView.isHidden = enable
        gradientLabel.alpha = enable ? 0 : 1
    }
    
    func configureCollectionView() {
        dataSource = MonthSelectorDataSource()
        dataSource?.configureTitles()
        dataSource?.delegate = self
        
        collectionView.dataSource = dataSource
        collectionView.delegate = dataSource
    }
    
    @IBAction func showDateSelector(_ sender: Any) {
        self.pickerViewBottom.constant = pickerViewBottom.constant != -10 ? -10 : -220
        
        if #available(iOS 10.0, *) {
            UIViewPropertyAnimator.runningPropertyAnimator(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: {
                self.view.layoutIfNeeded()
            }, completion: nil)
        } else {
            UIView.animate(withDuration: 0.3, animations: { 
                self.view.layoutIfNeeded()
            })
        }
    }
    
    @IBAction func updateDate(_ sender: Any) {
        initParser(dateToSearch: datePicker.date)
        
        showDateSelector(dateLabel)
    }
    
    @IBAction func didChangeDate(_ sender: UIDatePicker) {
        initParser(dateToSearch: sender.date)
    }
    
    @IBAction func showPreviousDay(_ sender: Any) {
        FIRAnalytics.logEvent(withName: "searchDay", parameters: [
            "tap": "previous" as NSObject
            ])
        
        currentDate = currentDate.addingTimeInterval(-24 * 60 * 60)
        prepareDay()
    }
    
    @IBAction func showNextDay(_ sender: Any) {
        FIRAnalytics.logEvent(withName: "searchDay", parameters: [
            "tap": "next" as NSObject
            ])
        
        currentDate = currentDate.addingTimeInterval(24 * 60 * 60)
        prepareDay()
    }
    
    @IBAction func showToday(_ sender: Any) {
        FIRAnalytics.logEvent(withName: "searchDay", parameters: [
            "tap": "today" as NSObject
            ])
        
        currentDate = Date()
        prepareDay()
    }
    
    func enableNextDay() -> Bool {
        
        let calendar = EcoFormatter.calendar
        
        let result = calendar.startOfDay(for: currentDate).compare(calendar.startOfDay(for: Date()))
        
        guard result == .orderedSame else {
            return result == .orderedAscending
        }
        
        guard let newDataDate = calendar.date(bySettingHour: 20, minute: 30, second: 0, of: Date()) else {
            return false }
        
        return Date().compare(newDataDate) == .orderedDescending
    }
    
    func prepareDay() {
        chartView.clearValues()
        initParser(dateToSearch: currentDate)
    }
    
    @IBAction func dragLeftHandler(_ sender: UIPanGestureRecognizer) {
        
        FIRAnalytics.logEvent(withName: "hourSelector", parameters: [
            "handler": "left" as NSObject
            ])
        
        let translation = sender.translation(in: self.view)
        
        var frame = hoursDraggingView.frame
        frame.origin.x += translation.x
        frame.size.width -= translation.x
        
        guard frame.size.width >= hoursSeparatorView.minDragSize
            else { return }
        
        hoursDraggingView.frame = frame
        
        var left = leftHandle.frame
        left.origin.x += translation.x
        leftHandle.frame = left
        
        if sender.state == .ended {
            let (index, newX) = hoursSeparatorView.roundPositionToSeparators(hoursDraggingView.frame.origin.x)
            frame.size.width += frame.origin.x - newX
            frame.origin.x = newX
            
            leftHandle.center.x = newX
            hoursDraggingView.frame = frame
            
            let startDate = Date.init(timeInterval: TimeInterval(index * 3600), since: EcoFormatter.calendar.startOfDay(for: Date()))
            
            Settings.storeNotificationsTime(startTime: startDate)
            
            configureUI()
        }
        
        sender.setTranslation(.zero, in: self.view)
    }
    
    @IBAction func dragRightHandler(_ sender: UIPanGestureRecognizer) {
        
        FIRAnalytics.logEvent(withName: "hourSelector", parameters: [
            "handler": "right" as NSObject
            ])
        
        let translation = sender.translation(in: self.view)
        
        var frame = hoursDraggingView.frame
        frame.size.width += translation.x
        
        guard frame.size.width >= hoursSeparatorView.minDragSize else { return }
        
        hoursDraggingView.frame = frame
        
        var right = rightHandle.frame
        right.origin.x += translation.x
        rightHandle.frame = right
        
        if sender.state == .ended {
            let (index, newX) = hoursSeparatorView.roundPositionToSeparators(hoursDraggingView.frame.maxX)
            frame.size.width = newX - hoursDraggingView.frame.origin.x
            
            rightHandle.center.x = newX
            hoursDraggingView.frame = frame
            
            let endDate = Date.init(timeInterval: TimeInterval(index * 3600), since: EcoFormatter.calendar.startOfDay(for: Date()))
            
            Settings.storeNotificationsTime(endTime: endDate)
            
            configureUI()
        }
        
        sender.setTranslation(.zero, in: self.view)
    }
    
    func adjustHandlers() {
        var frame = hoursDraggingView.frame
        
        let (_, newX) = hoursSeparatorView.roundPositionToSeparators(hoursDraggingView.frame.origin.x)
        frame.size.width += frame.origin.x - newX
        frame.origin.x = newX
        
        leftHandle.center.x = newX
        hoursDraggingView.frame = frame
        
        let (_, newXRight) = hoursSeparatorView.roundPositionToSeparators(frame.maxX)
        frame.size.width = newXRight - hoursDraggingView.frame.origin.x
        
        rightHandle.center.x = newXRight
        hoursDraggingView.frame = frame
    }
    
    func loadHandlersPosition() {
        let (start, end) = Settings.retrieveInterval()
        self.leftHandle.center.x = self.hoursSeparatorView.origin(for: start).x
        self.rightHandle.center.x = self.hoursSeparatorView.origin(for: end).x
        
        var frame = self.hoursDraggingView.frame
        frame.origin.x = self.leftHandle.center.x
        frame.size.width = self.rightHandle.center.x - self.leftHandle.center.x
        self.hoursDraggingView.frame = frame
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        ((segue.destination as? UINavigationController)?
            .topViewController as? NotificationSettingsViewController)?
            .delegate = self
    }
}

extension MainViewController: NotificationSettingsDelegate {
    func convenientHoursdidChange() {
        loadHandlersPosition()
        configureUI()
    }
}

extension MainViewController: ParserDelegate {
    
    func didFinishParsing(withPrices prices: PricesSource) {
        configureSearchUI(false)
        
        self.prices = prices
        configureUI()
    }
    
    func configureUnavailableLabel(_ label: UILabel) {
        label.text = NSLocalizedString("No Disponible", comment: "")
    }
    
    func configureUI() {
        
        guard let prices = prices else { return }
        
       configureLabels()
        
        guard prices.data.count > 0 else { return }
        
        chartView.configure(withPrices: prices)
        
        collectionView.selectItem(at: IndexPath(row: dataSource!.info!.count - 1, section: 0), animated: false, scrollPosition: .right)
    }
    
    func configureLabels() {
        if let hour = prices?.getBestHour() {
            bestTimeLabel.text = "\(hour):00"
        } else {
            configureUnavailableLabel(bestTimeLabel)
        }
        
        if let hour = prices?.getWorstHour() {
            worstTimeLabel.text = "\(hour):00"
        } else {
            configureUnavailableLabel(worstTimeLabel)
        }
        
        if let hour = prices?.getConvenientHour() {
            convenientTimeLabel.text = "\(hour):00"
        } else {
            configureUnavailableLabel(convenientTimeLabel)
        }
    }
}

extension MainViewController: MonthSelectorDataSourceDelegate {
    
    func monthTap(_ position: Int) {
        var dateComponents = DateComponents()
        dateComponents.year =
            (position + 1) <= Calendar.current.component(.month, from: Date())
            ? 2017
            : 2016
        dateComponents.month = position + 1
        
        guard let newDate = EcoFormatter.calendar.date(from: dateComponents) else { return }
        
        FIRAnalytics.logEvent(withName: "monthTapped", parameters: [
            "month": "\(position + 1)" as NSObject
            ])
        
        currentDate = newDate
        prepareDay()
    }
}

extension MainViewController: HoursSeparatorDelegate {
    func didLoad() {
        loadHandlersPosition()
    }
}
