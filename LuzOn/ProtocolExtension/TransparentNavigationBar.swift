//
//  TransparentNavigationBar.swift
//  AhorraLuz
//
//  Created by Alejandro Portillo Guerrero on 13/02/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit

protocol TransparentNavigationBar {
    func makeNavigationBarTransparent()
}

extension TransparentNavigationBar where Self: UIViewController {
    func makeNavigationBarTransparent() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.isTranslucent = true
    }
}
