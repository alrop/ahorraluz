//
//  ReusableView.swift
//  doozy-ios-app
//
//  Created by Alejandro Portillo Guerrero on 16/09/16.
//  Copyright © 2016 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit

protocol ReusableView: class {
    static var defaultReuseIdentifier: String { get }
}

extension ReusableView where Self: UIView {
    static var defaultReuseIdentifier: String {
        return String(describing: self)
    }
}

extension UICollectionViewCell: ReusableView {}
extension UITableViewCell: ReusableView {}
