//
//  StoryboardIdentifiable.swift
//  doozy-ios-app
//
//  Created by Alejandro Guerrero on 31/08/2016.
//  Copyright © 2016 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit

protocol StoryboardIdentifiable {
    static var storyboardIdentifier: String { get }
}
