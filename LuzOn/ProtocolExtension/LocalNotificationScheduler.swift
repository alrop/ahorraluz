//
//  LocalNotificationScheduler.swift
//  doozy-ios-app
//
//  Created by Alejandro Guerrero on 24/10/2016.
//  Copyright © 2016 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit
import UserNotifications

protocol LocalNotificationScheduler {
    @available(iOS 10.0, *)
    func scheduleLocalNotification(notification: LocalNotification)
    
    @available(iOS 9, *)
    func scheduleLocalNotification(notification: UILocalNotification)
    
    func cancelLocalNotification(identifier: String)
}

extension LocalNotificationScheduler {
    @available(iOS 10.0, *)
    func scheduleLocalNotification(notification: LocalNotification) {
        
        let center = UNUserNotificationCenter.current()
        center.add(notification.request) { (error) in
            if let error = error {
                print("There was an error adding localNotification: \(type(of: notification)): \(error)")
            }
        }
    }
    
    @available(iOS 9, *)
    func scheduleLocalNotification(notification: UILocalNotification) {
        UIApplication.shared.scheduleLocalNotification(notification)
    }
    
    func cancelLocalNotification(identifier: String) {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [identifier])
        } else {
            UIApplication.shared.cancelAllLocalNotifications()
        }
    }
}
