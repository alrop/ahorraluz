//
//  NibLoadableView.swift
//  doozy-ios-app
//
//  Created by Alejandro Portillo Guerrero on 16/09/16.
//  Copyright © 2016 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit

protocol NibLoadableView: class {
    static var nibName: String { get }
}

extension NibLoadableView where Self: UIView {
    static var nibName: String {
        return String(describing: self)
    }
}
