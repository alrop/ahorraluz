//
//  MonthSelectorCollectionViewCell.swift
//  AhorraLuz
//
//  Created by Alejandro Portillo Guerrero on 13/02/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit

class MonthSelectorCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    var monthPosition: Int!
    
    override func awakeFromNib() {
        titleLabel.textColor = .white
        
        layer.addBorder(edge: .right, color: .white, thickness: 1, alpha: 0.2)
    }
}
