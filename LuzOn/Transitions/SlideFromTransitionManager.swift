//
//  SlideFromTransitionManager.swift
//  AhorraLuz
//
//  Created by Alejandro Portillo Guerrero on 18/02/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit

enum SlideFromSide: Int {
    case left
    case right
    case top
    case bottom
}

class SlideFromTransitionManager: NSObject, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {
    
    var slideFrom = SlideFromSide.left
    
    private var presenting = false
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        
        let screens: (from: UIViewController, to: UIViewController) = (
            transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!,
            transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        )
        
        let v1 = self.presenting ? screens.from : screens.to
        let v2 = self.presenting ? screens.to : screens.from
        
        let containerView = transitionContext.containerView
        
        let snapshotView = v2.view.resizableSnapshotView(from: v2.view.frame, afterScreenUpdates: true, withCapInsets: .zero)
        
//        snapshotView?.frame.origin = self.presenting ? CGPoint(x: v1.view.frame.size.width, y: 0) : v2.view.frame.origin
        if self.presenting {
            var translationX: CGFloat = 0.0
            var translationY: CGFloat = 0.0
            
            switch slideFrom {
            case .left:
                translationX = -containerView.frame.width
            case .right:
                translationX = containerView.frame.width
            case .top:
                translationY = -containerView.frame.height
            case .bottom:
                translationY = containerView.frame.height
            }
            
            snapshotView?.transform = CGAffineTransform(translationX: translationX, y: translationY)
        }

        containerView.addSubview(snapshotView!)
        
        v2.view.alpha = 0.0
        containerView.addSubview(v2.view)
        
        let duration = self.transitionDuration(using: transitionContext)

//        UIView.animate(withDuration: duration, animations: {
//        UIView.animate(withDuration: duration, delay: 0, options: .curveEaseIn, animations: {
        UIView.animate(withDuration: duration, delay: 0.0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.0, options: [], animations: {
            
//            snapshotView?.transform = CGAffineTransform(translationX: (self.presenting ? 0 : 1) * containerView.frame.width, y: 0.0)
            snapshotView?.transform = self.presenting ? v2.view.transform : v1.view.transform
            
            
//            snapshotView?.frame.origin = self.presenting ? v1.view.frame.origin : CGPoint(x: v1.view.frame.size.width, y: 0)
        }) { (finished) in
            snapshotView?.removeFromSuperview()
            v2.view.alpha = 1.0
            transitionContext.completeTransition(finished)

//            UIApplication.shared.keyWindow?.addSubview(v2.view)
        }
        
    }
    
    // return how many seconds the transiton animation will take
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.presenting = true
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.presenting = false
        return self
    }
}

