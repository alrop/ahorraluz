//
//  GradientView.swift
//  AhorraLuz
//
//  Created by Alejandro Portillo Guerrero on 26/01/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit

//@IBDesignable
class GradientView: UIView {
    @IBInspectable var topColor: UIColor = UIColor.white {
        didSet {
            setNeedsLayout()
        }
    }

    @IBInspectable var midColor: UIColor? {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var bottomColor: UIColor = UIColor.white {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var startPoint: CGPoint = CGPoint(x: 0.5, y: 0) {
        didSet {
            setNeedsLayout()
        }
    }
    
    @IBInspectable var endPoint: CGPoint = CGPoint(x: 0.5, y: 1) {
        didSet {
            setNeedsLayout()
        }
    }
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    public required init(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)!
        setNeedsLayout()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setNeedsLayout()
    }
    
    public override func tintColorDidChange() {
        super.tintColorDidChange()
        setNeedsLayout()
    }
    
    override func layoutSubviews() {
        let gradientLayer = layer as! CAGradientLayer
        var colors: [CGColor]
        
        if let midColor = midColor {
            colors = [topColor.cgColor, midColor.cgColor, bottomColor.cgColor]
        } else {
            colors = [topColor.cgColor, bottomColor.cgColor]
        }
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint
        
        gradientLayer.colors = colors
    }
}
