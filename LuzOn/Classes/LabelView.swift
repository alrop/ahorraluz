//
//  LabelView.swift
//  AhorraLuz
//
//  Created by Alejandro Portillo Guerrero on 27/01/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit

class LabelView: UIView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func configureViews() {
        
        guard subviews.count == 0 else { return }
        
        let numberOfSections: Int = 12
        let separators: Int = numberOfSections - 1
        
        let size = (CGFloat(frame.width) - CGFloat(separators)) / CGFloat(numberOfSections)
        
        for i in 0..<numberOfSections {
            
            let x = CGFloat((size + 1) * CGFloat(i))
            
            let label = UILabel(frame: CGRect(x: x, y: 0.0, width: size, height: frame.height))
            label.text = "\(i * 2 + 1)"
            label.textAlignment = .center
            label.backgroundColor = .clear
            label.textColor = .white
            label.font = UIFont(name: "Helvetica", size: 12)
            addSubview(label)
            
            if i < numberOfSections - 1 {
                let separator = UIView(frame: CGRect(x: x + size, y: CGFloat(0), width: 1, height: frame.height))
                separator.backgroundColor = .white
                addSubview(separator)
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        configureViews()
    }
}
