//
//  EcoFormatter.swift
//  AhorraLuz
//
//  Created by Alejandro Portillo Guerrero on 13/02/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import Foundation

class EcoFormatter {
    static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        formatter.calendar = calendar
        return formatter
    }()
    
    static let visualFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd, YYYY"
        formatter.calendar = calendar
        return formatter
    }()
    
    static let calendar: Calendar = {
        var calendar = Calendar.autoupdatingCurrent
        calendar.timeZone = TimeZone.autoupdatingCurrent
        return calendar
    }()
}
