//
//  RotationalImage.swift
//  AhorraLuz
//
//  Created by Alejandro Portillo Guerrero on 26/01/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit

//@IBDesignable

class EditableView: UIButton {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var rotation: CGFloat = 0.0 {
        didSet {
            self.transform = CGAffineTransform(rotationAngle: rotation * CGFloat.pi / 180)
        }
    }
}
