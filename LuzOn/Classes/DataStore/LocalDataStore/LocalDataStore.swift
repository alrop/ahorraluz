//
//  LocalDataStore.swift
//  doozy-ios-app
//
//  Created by Alejandro Portillo Guerrero on 24/01/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import Foundation

class LocalDataStore: Any {
    let userDefaults = UserDefaults.standard
    
    func store(_ value: Any, key: String) {
        userDefaults.set(value, forKey: key)
    }
    
    func bool(forKey key: String) -> Bool {
        return userDefaults.bool(forKey: key)
    }
    
    func dict(forKey key: String) -> [String: Any]? {
        return userDefaults.dictionary(forKey: key)
    }
    
    func string(forKey key: String) -> String? {
        return userDefaults.string(forKey: key)
    }
    
    func set(_ value: Any, key: String) {
        userDefaults.set(value, forKey: key)
    }
    
    func remove(key: String) {
        userDefaults.removeObject(forKey: key)
    }
}
