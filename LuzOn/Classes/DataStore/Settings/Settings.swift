//
//  Settings.swift
//  doozy-ios-app
//
//  Created by Alejandro Portillo Guerrero on 24/01/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import Foundation


class Settings {
    
    fileprivate static let notificationsEnableKey = "Notifications.On"
    fileprivate static let notificationsTimeKey = "Settings.ConvenientHours"
    
    fileprivate static let startKey = "startKey"
    fileprivate static let endKey = "endKey"
    
    fileprivate static let localDataStore = LocalDataStore()
    
    static func storeNotificationsEnable(_ enable: Bool) {
        localDataStore.store(enable, key: notificationsEnableKey)
    }
    
    static func storeNotificationsTime(startTime: Date? = nil, endTime: Date? = nil) {
        
        let (start, end) = Settings.getConvenientHours()
        
        let dict = [startKey: (startTime ?? start), endKey: (endTime ?? end)]
        
        localDataStore.store(dict, key: notificationsTimeKey)
    }
    
    static func isEnabled() -> Bool {
        return localDataStore.bool(forKey: notificationsEnableKey)
    }
    
    static func getConvenientHours() -> (Date, Date) {
        
        guard let dict = localDataStore.dict(forKey: notificationsTimeKey) else {
            return defaultConvenientHours()
        }
        
        guard let start = dict[startKey] as? Date else { return defaultConvenientHours() }
        guard var end = dict[endKey] as? Date else { return defaultConvenientHours() }
        
        if end < start {
            end = start
        }
        
        return (start, end)
    }
    
    static func defaultConvenientHours() -> (Date, Date) {
        let midnight = Date.getMidnight()
        
        let start = Date(timeInterval: 28800, since: midnight)
        let end = Date(timeInterval: 82800, since: midnight)
        
        return (start, end)
    }
    
    static func retrieveInterval() -> (Int, Int) {
        let (startDate, endDate) = Settings.getConvenientHours()
        
        let start = EcoFormatter.calendar.dateComponents([.hour], from: startDate).hour ?? 8
        var end = EcoFormatter.calendar.dateComponents([.hour], from: endDate).hour ?? 24
        
        if end == 0 {
            end = 24
        } else if end < start {
            end = start
        }
        
        return (start, end)
    }
}
