//
//  HoursSeparatorView.swift
//  LuzOn
//
//  Created by Alejandro Portillo Guerrero on 07/04/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit

protocol HoursSeparatorDelegate: class {
    func didLoad()
}

class HoursSeparatorView: UIView {
    
    var minDragSize: CGFloat = 0
    
    weak var delegate: HoursSeparatorDelegate?
    
    private var selectorSpan: CGFloat = 0 {
        didSet {
            minDragSize = selectorSpan * 2
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func configureViews() {
        let numberOfSections: Int = 24
        
        guard subviews.count == 0 else { return }
        
        selectorSpan = (CGFloat(frame.width) - CGFloat(numberOfSections - 1)) / CGFloat(numberOfSections)
        
        for i in 0...numberOfSections {
            
            let x = CGFloat((selectorSpan + 1) * CGFloat(i)) - 1
            let separator = UIView(frame: CGRect(x: x, y: 0.0, width: 1, height: frame.height))
            separator.backgroundColor = UIColor.white.withAlphaComponent(0.2)
            addSubview(separator)
        }
        
        delegate?.didLoad()
    }
    
    func roundPositionToSeparators(_ position: CGFloat) -> (Int, CGFloat) {
        guard let info = subviews.enumerated().min(by: {abs($0.1.frame.origin.x - position) < abs($1.1.frame.origin.x - position) }) else {
            return (0, position)
        }
        
        return (info.0, info.1.frame.origin.x)
    }
    
    func origin(for index: Int) -> CGPoint {
        guard index < subviews.count else { return .zero }
        
        return subviews[index].frame.origin
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        configureViews()
    }
}
