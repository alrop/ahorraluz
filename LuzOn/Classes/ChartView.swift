//
//  ChartView.swift
//  AhorraLuz
//
//  Created by Alejandro Portillo Guerrero on 14/02/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import Foundation
import Charts


class ChartView: LineChartView {
    var pricesSource: PricesSource?
    
    func configure(withPrices pricesData: PricesSource) {
        self.pricesSource = pricesData
        
        guard let pricesSource = pricesSource else { return }
        
        guard pricesSource.data.count > 0 else { return }
        
        var dataEntries: [ChartDataEntry] = []
        for i in 0..<pricesSource.data.count {
            let dataEntry = ChartDataEntry(x: Double(i), y: pricesSource.dataNormalized[i])
            dataEntries.append(dataEntry)
        }
        
        let lineChartDataSet = LineChartDataSet(values: dataEntries, label: nil)
        lineChartDataSet.drawCirclesEnabled = false
        lineChartDataSet.drawValuesEnabled = false
        lineChartDataSet.mode = .cubicBezier
        lineChartDataSet.cubicIntensity = 0.15
        
        let gradientColors = [
            ChartColorTemplates.colorFromString("#FBDA61").cgColor,
            ChartColorTemplates.colorFromString("#F89C3B").cgColor
            ] as CFArray
        
        let colorLocations:[CGFloat] = [1.0, 0.0] // Positioning of the gradient
        let gradient = CGGradient.init(colorsSpace: CGColorSpaceCreateDeviceRGB(), colors: gradientColors, locations: colorLocations)
        lineChartDataSet.fill = Fill.fillWithLinearGradient(gradient!, angle: 90.0)
        lineChartDataSet.drawFilledEnabled = true
        lineChartDataSet.fillAlpha = 1
        lineChartDataSet.lineWidth = 0
        
        
        let lineChartData = LineChartData(dataSet: lineChartDataSet)
        
        xAxis.enabled = false
        leftAxis.enabled = false
        rightAxis.enabled = false
        legend.enabled = false
        doubleTapToZoomEnabled = false
        dragEnabled = false
        pinchZoomEnabled = false
        scaleXEnabled = false
        scaleYEnabled = false
        minOffset = 0
        extraBottomOffset = 0
        extraTopOffset = 0
        chartDescription?.text = ""
        drawBordersEnabled = false
        minOffset = 0
        data = lineChartData
        configureLimitLines()
    }
    
 
    func configureLimitLines() {
        leftAxis.removeAllLimitLines()
        leftAxis.gridLineDashLengths = [5, 5]
        leftAxis.drawZeroLineEnabled = false
        leftAxis.drawLimitLinesBehindDataEnabled = false
        
//        leftAxis.axisMinimum = 0
//        leftAxis.axisMaximum = pricesSource!.dataNormalized.max()!
        
        guard let pricesSource = pricesSource else { return }
        
        if let index = pricesSource.getConvenientHour() {
            let title = pricesSource.data[index]
            let value = pricesSource.dataNormalized[index]
            leftAxis.addLimitLine(createLimitLine(value: value, colorLine: .orange, text: String(format: "%.3g €/Kw", title)))
        }
        
        if let index = pricesSource.getBestHour() {
            let title = pricesSource.data[index]
            let value = pricesSource.dataNormalized[index]
            leftAxis.addLimitLine(createLimitLine(value: value, colorLine: .green, text: String(format: "%.3g €/Kw", title)))
        }
        
        if let index = pricesSource.getWorstHour() {
            let title = pricesSource.data[index]
            let value = pricesSource.dataNormalized[index]
            leftAxis.addLimitLine(createLimitLine(value: value, colorLine: .red, text: String(format: "%.3g €/Kw", title)))
        }
    }
}
extension ChartView {
    
    func createLimitLine(value: Double, colorLine: UIColor, text: String = "") -> ChartLimitLine {
        let limitLine = ChartLimitLine(limit: value, label: text)
        limitLine.lineWidth = 2
        limitLine.lineDashLengths = [5, 3]
        limitLine.labelPosition = .rightTop
        limitLine.valueFont = UIFont.systemFont(ofSize: 10)
        limitLine.lineColor = colorLine
        limitLine.valueTextColor = .white
        
        return limitLine
    }
    
    func addLimitLineImage(_ image: UIImage, value: Double, position: Int) {
        let imageView = UIImageView(image: image)
        var origin = getImagePos(forValue: value, atPosition: position)
        origin.y -= imageView.frame.size.height
        
        imageView.frame.origin = origin

        imageView.backgroundColor = .black
        addSubview(imageView)
    }
    
    func getImagePos(forValue value: Double, atPosition position: Int) -> CGPoint {
        guard let max = pricesSource?.dataNormalized.max() else { return .zero }
        
        let y = self.frame.height - (self.frame.height * CGFloat(value) / CGFloat(max))
        return CGPoint(x: self.frame.width - 90, y: y)
    }
}
