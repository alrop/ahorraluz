//
//  ConsejosPageViewController.swift
//  doozy-ios-app
//
//  Created by Alejandro Portillo Guerrero on 16/02/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit

class ConsejosPageViewController: UIPageViewController {
    
    internal var currentPage: Int = 0
    
    internal var imagesViewControllers = [UIViewController]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        
        var images = [#imageLiteral(resourceName: "lavadora"), #imageLiteral(resourceName: "horno"), #imageLiteral(resourceName: "regleta"), #imageLiteral(resourceName: "bombilla"), #imageLiteral(resourceName: "aire_acondicionado"), #imageLiteral(resourceName: "logo")]
        var names = [
            "Ponga la lavadora y lavavajillas en la hora más barata oal menos en la hora óptima. Use la lavadora con programas en frío siempre que sea posible.",
            "El horno es un gran consumidor de energía, evite usarlo en las horas más caras.",
            "No mantenga aparatos en stand-by, su consumo puede suponer hasta un 10% de la factura anual.",
            "Sustituya las bombillas tradicionales por lamparas led, éstas pueden suponer un ahorro de hasta un 80% en comparación con las de bajo consumo tradicionales.",
            "Mantenga la temperatura del aire acondicionado entre 23 y 25 grados centígrados. Apáguelo si es posible en las horas más caras.",
            "Revise periódicamente la evolución de los precios para gestionar mejor su gasto cada día."]
        
        for i in 0..<images.count {
            let vc: ImageViewController = UIStoryboard(storyboard: .Main).instantiateViewController()
            vc.image = images[i]
            vc.name = names[i]
            
            imagesViewControllers.append(vc)
        }
        
        setViewControllers([imagesViewControllers[0]], direction: .forward, animated: true, completion: nil)
    }
    
    func setPage(_ position: Int) {
        currentPage = position
        
        setViewControllers([imagesViewControllers[position]], direction: .forward, animated: true, completion: nil)
    }
}

extension ConsejosPageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let index = imagesViewControllers.index(of: viewController) else { return nil }
        
        guard index + 1 < imagesViewControllers.count else { return nil }
        
        return imagesViewControllers[index + 1]
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = imagesViewControllers.index(of: viewController) else { return nil }
        
        guard index > 0 else { return nil }
        
        return imagesViewControllers[index - 1]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return imagesViewControllers.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
}
