//
//  InformationViewController.swift
//  AhorraLuz
//
//  Created by Alejandro Portillo Guerrero on 01/04/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class InformationViewController: UIViewController,  TransparentNavigationBar {
    
    @IBOutlet weak var container: UIView!
    
    var consejos: ConsejosPageViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeNavigationBarTransparent()
        
        FIRAnalytics.logEvent(withName: "consejos_section", parameters: [:])
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: #imageLiteral(resourceName: "closeCross"),
            style: .plain,
            target: self,
            action: #selector(dismissViewController)
        )
        
        consejos = ConsejosPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal)
        
        willMove(toParentViewController: consejos)
        addChildViewController(consejos)
        container.addSubview(consejos.view)
        didMove(toParentViewController: self)
        
        consejos.view.frame.size = container.frame.size
    }
    
    func dismissViewController() {
        dismiss(animated: true, completion: nil)
    }
}
