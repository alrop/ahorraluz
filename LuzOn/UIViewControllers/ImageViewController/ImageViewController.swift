//
//  ImageViewController.swift
//  doozy-ios-app
//
//  Created by Alejandro Portillo Guerrero on 23/01/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var image: UIImage?
    var name: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = name
        imageView.image = image
    }
}
