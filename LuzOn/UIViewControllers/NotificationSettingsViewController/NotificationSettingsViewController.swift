//
//  NotificationSettingsViewController.swift
//  AhorraLuz
//
//  Created by Alejandro Portillo Guerrero on 17/02/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit
import UserNotifications
import FirebaseAnalytics

protocol NotificationSettingsDelegate: class {
    func convenientHoursdidChange()
}

class NotificationSettingsViewController: UIViewController, LocalNotificationScheduler, TransparentNavigationBar {
    @IBOutlet weak var startPicker: UIDatePicker!
    @IBOutlet weak var endPicker: UIDatePicker!
    @IBOutlet weak var saveSwitch: UISwitch!
    
    weak var delegate: NotificationSettingsDelegate?
    
    override func viewDidLoad() {
        configurePickers()
        makeNavigationBarTransparent()
        
        FIRAnalytics.logEvent(withName: "notifications_section", parameters: [:])
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: #imageLiteral(resourceName: "closeCross"),
            style: .plain,
            target: self,
            action: #selector(dismissViewController)
        )
    }
    
    func dismissViewController() {
        dismiss(animated: true, completion: nil)
    }
    
    func configurePickers() {
        startPicker.setValue(UIColor.white, forKey: "textColor")
        startPicker.minuteInterval = 0
        endPicker.setValue(UIColor.white, forKey: "textColor")
        
        saveSwitch.isOn = Settings.isEnabled()
        
        let (start, end) = Settings.getConvenientHours()
        
        startPicker.date = start
        endPicker.date = end
    }
    
    @IBAction func saveSettings(_ sender: Any) {
        
        Settings.storeNotificationsEnable(saveSwitch.isOn)
        
        FIRAnalytics.logEvent(withName: "notifications_active", parameters: [
            "enable": "\(saveSwitch.isOn)" as NSObject,
            ])
        
        guard saveSwitch.isOn else {
            if #available(iOS 10.0, *) {
                cancelLocalNotification(identifier: String(describing: DailyNotification.self))
            } else {
                cancelLocalNotification(identifier: "")
            }
            
            return
        }
        
        requestPermissionsIfNeeded()
        
        let date = Date(timeInterval: 75600, since: EcoFormatter.calendar.startOfDay(for: Date()))
        
        if #available(iOS 10.0, *) {
            let notification = DailyNotification(date: date)
            scheduleLocalNotification(notification: notification)
        } else {
            let notification = DailyNotificationiOS9(date: date)
            scheduleLocalNotification(notification: notification)
        }
    }
    
    func requestPermissionsIfNeeded() {
        let notificationsManager = NotificationsManager()
        notificationsManager.requestPermission()
    }
    
    @IBAction func timeDidChange(_ sender: UIDatePicker) {
        
        if Date.calendar.component(.hour, from: endPicker.date) - Date.calendar.component(.hour, from: startPicker.date) < 2 {
            endPicker.setDate(Date.calendar.date(byAdding: .hour,
                                                 value: 2,
                                                 to: startPicker.date) ?? startPicker.date,
                              animated: true)
        }
        
        var endDate = endPicker.date
        
        if sender == endPicker {
            if endPicker.date == Date.getMidnight() {
                endDate = endPicker.calendar.date(byAdding: .day, value: 1, to: endPicker.date) ?? endDate
            }
        }
        
        trackAnalytics()
        
        Settings.storeNotificationsTime(startTime: startPicker.date,
                                        endTime: endDate)
        
        delegate?.convenientHoursdidChange()
    }
    
    func trackAnalytics() {
        let start = EcoFormatter.calendar.dateComponents([.hour, .minute], from: startPicker.date)
        let end = EcoFormatter.calendar.dateComponents([.hour, .minute], from: endPicker.date)
        
        guard let sHour = start.hour, let sMinute = start.minute, let eHour = end.hour, let eMinute = end.minute else { return }
        
        FIRAnalytics.setUserPropertyString("\(sHour):\(sMinute)-\(eHour):\(eMinute)", forName: "bestTime")
    }
}
