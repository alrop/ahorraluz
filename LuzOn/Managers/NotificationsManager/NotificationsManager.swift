//
//  NotificationsManager.swift
//  doozy-ios-app
//
//  Created by Alejandro Portillo Guerrero on 15/02/2017.
//  Copyright © 2017 Alejandro Portillo Guerrero. All rights reserved.
//

import UIKit
import UserNotifications

protocol NotificationsManagerDelegate: class {
    func notificationReceived()
}

protocol NotificationsManagerSetUpDelegate: class {
    func notificationsDidRequest()
}

class NotificationsManager: NSObject {
    
    static var permissionRequested: Bool = false
    
    weak var delegate: NotificationsManagerDelegate?
    weak var setUpDelegate: NotificationsManagerSetUpDelegate?
    
    func setUp() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                if settings.authorizationStatus == .authorized {
                    self.requestPermission()
                }
            }
        } else {
            if UIApplication.shared.currentUserNotificationSettings != nil {
               self.requestPermission()
            }
        }
    }
    
    func requestPermission() {
        
        guard !NotificationsManager.permissionRequested else { return }
        
        NotificationsManager.permissionRequested = true
        
        if #available(iOS 10.0, *) {
            let authOptions : UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: { (_, _) in
            })
            
            UIApplication.shared.registerForRemoteNotifications()
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        
        setUpDelegate?.notificationsDidRequest()
    }
}

// MARK: - Push Notification Handlers

@available(iOS 10.0, *)
extension NotificationsManager : UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        print("Identifier: ", notification.request.identifier)
        print("userInfo: ", userInfo)
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Message ID: \(response)")
        delegate?.notificationReceived()
        
        completionHandler()
    }
}
